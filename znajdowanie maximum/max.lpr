program maksimum;
var
  tab : array[1..100] of integer;
  i : integer;
  max:integer;
begin
  randomize;
  for i := 1 to 100 do
    tab[i] := random(200)+1;
  writeln('Tablica liczb nieuporzadkowanych');
  for i := 1 to 100 do
    write(tab[i],' ');
  //algorytm
  max:=tab[1];
  for i := 2 to 100 do
    if tab[i]>max then
      max := tab[i];
  //koniec algorytmu
  writeln;
  writeln('Max = ',max);
  readln;
end.

