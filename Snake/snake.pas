program snake;
uses crt;
const
  MAX_DLUGOSC_SNAKE = 20;
  MIN_DLUGOSC_SNAKE = 10;
type
  wsp = record
    x : integer;
    y : integer;
  end;
var
  klawisz : char;
  snake_tab : array[1..MAX_DLUGOSC_SNAKE] of wsp;
  i : integer;
begin
  textcolor(yellow);
  for i := 1 to MIN_DLUGOSC_SNAKE do begin
    snake_tab[i].x := 35+i;
    snake_tab[i].y := 12;
  end;
  for i := 1 to MIN_DLUGOSC_SNAKE do begin
    gotoxy(snake_tab[i].x,snake_tab[i].y);
    write(#219);
  end;
  repeat
    if keypressed then klawisz := readkey;
    case klawisz of
      's': gotoxy(wherex-1,wherey+1);
      'w': gotoxy(wherex-1,wherey-1);
      'a': gotoxy(wherex-2,wherey);
      'd': gotoxy(wherex,wherey);
    end;
    write(#219);
    delay(300);
  until klawisz = #27;
end.

