program scalanie;
uses
  sysutils;
const
  MAX = 50000;
type
  tablica = array[1..MAX] of integer;
var
  tab,tab_pom : tablica;
  i : integer;
  czas_pocz, czas_kon : real;
procedure scalanie(l : integer; s : integer; p : integer);
var
  i,j,k,m,n : integer;
begin
  i := l; j := s+1; k := 1;
  repeat
    if tab[i] < tab[j] then begin
      tab_pom[k] := tab[i];
      inc(i); end
    else begin
      tab_pom[k] := tab[j];
      inc(j);
    end;
    inc(k);
  until (i > s) or (j > p);
  for m := i to s do begin
    tab_pom[k] := tab[m];
    inc(k);
  end;
  for n := j to p do begin
    tab_pom[k] := tab[n];
    inc(k);
  end;
  for m :=0 to p-l do
    tab[l+m]:=tab_pom[m+1];
end;

procedure sortowanie_przez_scalanie(l : integer; p : integer);
var
  s : integer;
begin
  if l<p then begin
    s := (l+p) div 2;
    sortowanie_przez_scalanie(l,s);
    sortowanie_przez_scalanie(s+1,p);
    scalanie(l,s,p);
end;
end;

procedure sortowanie_babelkowe;
var
  i,j,pom : integer;
begin
  for i := 1 to MAX - 1 do
    for j := 1 to MAX - i do
      if tab[j] > tab[j+1] then begin
         pom := tab[j];
         tab[j] := tab[j+1];
         tab[j+1] := pom;
      end;
end;

begin
  randomize;
  for i := 1 to MAX do
    tab[i] := random(MAX)+1;
  writeln('Sortowanie przez scalanie...');
  czas_pocz := time;
  sortowanie_przez_scalanie(1,MAX);
  czas_kon := time;
  writeln('Posortowane przez scalanie w czasie ',timetostr(czas_kon-czas_pocz));
  for i := 1 to MAX do
    tab[i] := random(MAX)+1;
  writeln('Sortowanie przez babelkowe');
  czas_pocz := time;
  sortowanie_babelkowe;
  czas_kon := time;
  writeln('Posortowane babelkowo ',timetostr(czas_kon-czas_pocz));
  readln
end.

