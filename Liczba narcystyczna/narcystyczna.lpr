program liczba_narcystyczna;
var
  k,l,i,j : integer;
  narcystyczna : integer;
  tab : array[1..10] of integer;

function potega(p : integer; w : integer) : integer;
var
  wynik,i : integer;
begin
  wynik := 1;
  for i := 1 to w do
    wynik := wynik*p;
end;

begin
  readln(k);
  i := 0;
  l := k;
  while k > 0 do begin
    inc(i);
    tab[i] := k mod 10;
    k := k div 10;
  end;
  narcystyczna := 0;
  for j := 1 to i do
    narcystyczna := narcystyczna + potega(tab[j],3);
  if l = narcystyczna then
    writeln('Liczba ',l,' jest narcystyczna')
  else
    writeln('Liczba ',l,' nie jest narcystyczna');
  readln;
end.

